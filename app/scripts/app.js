'use strict';

/**
 * @ngdoc overview
 * @name ycombinatorMirrorApp
 * @description
 * # ycombinatorMirrorApp
 *
 * Main module of the application.
 */
angular
  .module('ycombinatorMirrorApp', [
    'ngResource',
    'ngRoute',
    'ngAnimate',
    'articles'
  ])
  .config(function ($routeProvider) {
    console.log('doot');
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutController',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .config(['$locationProvider', function ($locationProvider) {
    // $locationProvider.hashPrefix('');
    // $locationProvider.html5Mode(true);
  }]);

