'use strict';

/**
 * @ngdoc function
 * @name ycombinatorMirrorApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ycombinatorMirrorApp
 */
angular.module('ycombinatorMirrorApp')
  .controller('MainController', ['$scope', 'articleFetcher', function MainController ($scope, articleFetcher) {
    $scope.articles = [];
    
    articleFetcher.fetchTop25().then(function() {
      var top25 = articleFetcher.getTop25();
      for (var id of top25) {
        articleFetcher.fetchDetails(id).then(function(response) {
          var article = response.data;
          article.index = top25.indexOf(article.id);
          article.origin = new URL(article.url).origin;
          $scope.articles.push(article);
        });
      }
    });
  }]);
