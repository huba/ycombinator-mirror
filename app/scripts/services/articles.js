'use strict';

angular.module('articles', [])
  .factory('articleFetcher', ['$http', function($http) {
    var top25 = [];
    
    var fetchTop25 = function() {
      var url = 'https://hacker-news.firebaseio.com/v0/topstories.json';
      console.log('refreshing');
      return $http.get(url).then(function(response) {
        top25 = response.data.slice(0, 25);
      });
    };
    
    var fetchDetails = function(id) {
      var url = 'https://hacker-news.firebaseio.com/v0/item/' + id + '.json';
      return $http.get(url);
    };
    
    var getTop25 = function() {
      return top25;
    };
    
    return {
      fetchTop25: fetchTop25,
      getTop25: getTop25,
      fetchDetails: fetchDetails
    };
  }]);